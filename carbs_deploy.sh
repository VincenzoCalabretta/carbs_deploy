#!/bin/bash
# CARBS VERSION 0.01 - Vincenzo Calabretta
# pathkeeping ahahahaah... i dunno how to shellscript :(
START_PTH=$PWD
CARBS_DEP_DIR=$(dirname "$0")
# clone carbs dotfiles
sudo rm -r $HOME/.*
sudo rm -r $HOME/.cfg
sudo rm -r $HOME/.gitignore

echo ".cfg" >> .gitignore
echo ".gitconfig" >> .gitignore

git clone --depth 1 --bare https://gitlab.com/VincenzoCalabretta/carbs_dotfiles $HOME/.cfg 

function config {
   /usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME $@
}
echo "[user]" >> $HOME/.gitconfig
echo "name = Vincenzo Calabretta" >> $HOME/.gitconfig
echo "email = vincenzo.cal.main@gmail.com" >> $HOME/.gitconfig

mkdir -p "$HOME/.config-backup"
config checkout
if [ $? = 0 ]; then
  echo "Checked out config.";
  else
    echo "Backing up pre-existing dot files.";
    config checkout 2>&1 | grep -E "\s+\." | awk {'print $1'} | xargs -I{} mv {} .config-backup/{}

fi;
#config checkout
config config status.showUntrackedFiles no
#fix index
rm $HOME/.cfg/index
config reset HEAD

#give execution permission to scripts
chmod +x $HOME/.config/shell/profile


read -p "Press enter to continue"

#install yay
sudo pacman -S --needed git base-devel && git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si

#install packages from list 
yay --noconfirm -S - < $HOME/.config/carbs/pkgs.txt


#compile software that needs to be built from source
#---- DMENU --------------------------------------------------
#git clone git://git.suckless.org/dmenu
#git clone https://github.com/LukeSmithxyz/dmenu $HOME/.local/src/dmenu
#cd $HOME/.local/src/dmenu
#sudo make install

#---- DWM  ---------------------------------------------------
#git clone git://git.suckless.org/dwm
#git clone https://github.com/LukeSmithxyz/dwm $HOME/.local/src/dwm
#cd $HOME/.local/src/dwm
#sudo make install

#---- DWMBLOCKS ----------------------------------------------
#git clone https://github.com/LukeSmithxyz/dwmblocks $HOME/.local/src/dwmblocks
#cd $HOME/.local/src/dwmblocks
#sudo make install

#---- ST  ----------------------------------------------------
git clone git://git.suckless.org/st
git clone https://github.com/LukeSmithxyz/st $HOME/.local/src/st
cd $HOME/.local/src/st
sudo make install


# execute packer to install neovim packages


#other sys settings
#echo "$START_PTH/$CARBS_DEP_DIR/carbs_sudoas.sh"
sudo bash $START_PTH/$CARBS_DEP_DIR/carbs_sudoas.sh


echo "all done, hopefully"
